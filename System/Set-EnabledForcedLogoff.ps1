﻿## Script is for private repo. Remove this line if publishing to public
## In the public version the functions are sourced from the common directory in the repository. The code has been commented out.
## download the supporting functions and bring into session
## iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## The following url is the private repo.
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/entelechyit/Security/raw/master/Common/iregistry.ps1')

## SV-52860r1_rule : The regsitry HKLM:\System\CurrentControlSet\Services\LanManServer\Parameters\ needs to have the value name EnableForcedLogoff with the value of 1 of type DWORD 
## Users must not be permitted to remain logged on to the network after they have exceeded their permitted logon hours.  In many cases, this indicates that a user forgot to log off before leaving for the day.  However, it may also indicate that a user is attempting unauthorized access at a time when the system may be less closely monitored.  Forcibly disconnecting users when logon hours expire protects critical and sensitive network data from exposure to unauthorized personnel with physical access to the computer.' 
Ensure-ItemProperty -LiteralPath 'HKLM:\System\CurrentControlSet\Services\LanManServer\Parameters\' -Name 'EnableForcedLogoff' -Value '1'