﻿## Script is for private repo. Remove this line if publishing to public
## In the public version the functions are sourced from the common directory in the repository. The code has been commented out.
## download the supporting functions and bring into session
## iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## The following url is the private repo.
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/entelechyit/Security/raw/master/Common/iregistry.ps1')

## SV-52840r1_rule : The regsitry HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System\ needs to have the value name ShutdownWithoutLogon with the value of 0 of type DWORD 
## Displaying the shutdown button may allow individuals to shut down a system anonymously.  Only authenticated users should be allowed to shut down the system.  Preventing display of this button in the logon dialog box ensures that individuals who shut down the system are authorized and tracked in the systems Security event log. 
Ensure-ItemProperty -LiteralPath 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -Name 'ShutdownWithoutLogon' -Value '0'