﻿## Script is for private repo. Remove this line if publishing to public
## In the public version the functions are sourced from the common directory in the repository. The code has been commented out.
## download the supporting functions and bring into session
## iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## The following url is the private repo.
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/entelechyit/Security/raw/master/Common/iregistry.ps1')

## SV-52846r2_rule : The regsitry HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\ needs to have the value name CachedLogonsCount with the value of 4 of type String 
## The default Windows configuration caches the last logon credentials for users who log on interactively to a system.  This feature is provided for system availability reasons, such as the users machine being disconnected from the network or domain controllers being unavailable.  Even though the credential cache is well-protected, if a system is attacked, an unauthorized individual may isolate the password to a domain user account using a password-cracking program and gain access to the domain.
Ensure-ItemProperty -LiteralPath 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\' -Name 'CachedLogonsCount' -Value '4'
