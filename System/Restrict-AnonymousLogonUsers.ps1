﻿## Script is for private repo. Remove this line if publishing to public
## In the public version the functions are sourced from the common directory in the repository. The code has been commented out.
## download the supporting functions and bring into session
## iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## The following url is the private repo.
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/entelechyit/Security/raw/master/Common/iregistry.ps1')

## SV-52847r1_rule : The regsitry HKLM:\System\CurrentControlSet\Control\Lsa\ needs to have the value name RestrictAnonymous with the value of 1 of type DWORD 
## Allowing anonymous logon users (null session connections) to list all account names and enumerate all shared resources can provide a map of potential points to attack the system.
Ensure-ItemProperty -LiteralPath 'HKLM:\System\CurrentControlSet\Control\Lsa\' -Name 'RestrictAnonymous' -Value '1'