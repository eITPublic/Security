﻿## Script is for private repo. Remove this line if publishing to public
## In the public version the functions are sourced from the common directory in the repository. The code has been commented out.
## download the supporting functions and bring into session
## iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## The following url is the private repo.
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/entelechyit/Security/raw/master/Common/iregistry.ps1')

## SV-52869r2_rule 
## If this option is enabled, the Recovery Console does not require a password and automatically logs on to the system.  This could allow unauthorized administrative access to the system. The regsitry HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Setup\RecoveryConsole\ needs to have the value name SecurityLevel with the value of 0 of type DWORD.
Ensure-ItemProperty -LiteralPath 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Setup\RecoveryConsole\' -Name 'SecurityLevel' -Value '0'