﻿## Script is for private repo. Remove this line if publishing to public
## In the public version the functions are sourced from the common directory in the repository. The code has been commented out.
## download the supporting functions and bring into session
## iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## The following url is the private repo.
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/entelechyit/Security/raw/master/Common/iregistry.ps1')

## SV-52214r2_rule : The regsitry HKLM:\System\CurrentControlSet\Control\Print\Providers\LanMan Print Services\Servers\ needs to have the value name AddPrinterDrivers with the value of 1 of type DWORD 
## Allowing users to install drivers can introduce malware or cause the instability of a system.  Print driver installation should be restricted to administrators.' 
 Ensure-ItemProperty -LiteralPath 'HKLM:\System\CurrentControlSet\Control\Print\Providers\LanMan Print Services\Servers\' -Name 'AddPrinterDrivers' -Value '1'