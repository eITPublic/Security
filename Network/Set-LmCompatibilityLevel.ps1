﻿# download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## SV-52865r1_rule : The regsitry HKLM:\System\CurrentControlSet\Control\Lsa\ needs to have the value name LmCompatibilityLevel with the value of 5 of type DWORD 
## The Kerberos v5 authentication protocol is the default for authentication of users who are logging on to domain accounts.  NTLM, which is less secure, is retained in later Windows versions  for compatibility with clients and servers that are running earlier versions of Windows or applications that still use it.  It is also used to authenticate logons to stand-alone computers that are running later versions.
Ensure-ItemProperty -LiteralPath 'HKLM:\System\CurrentControlSet\Control\Lsa\' -Name 'LmCompatibilityLevel' -Value '5'