﻿ # download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/Test-IsAdmin.ps1')

if(Test-IsAdministrator){ 
  ## Disable SSL 1.0
  Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server" -name "Enabled" -value 0 -PropertyType "DWord"
  Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server" -name "DisabledByDefault" -value 1 -PropertyType "DWord"

}
else{
  Write-Warning "session is not running as administrator. Please start PowerShell as with an Administrator session"
}