﻿# download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## SV-52899r2_rule : The regsitry HKLM:\Software\Policies\Microsoft\Windows NT\Terminal Services\ needs to have the value name MinEncryptionLevel with the value of 3 of type DWORD
## Remote connections must be encrypted to prevent interception of data or sensitive information. Selecting High Level will ensure encryption of Remote Desktop Services sessions in both directions.
Ensure-ItemProperty -LiteralPath 'HKLM:\Software\Policies\Microsoft\Windows NT\Terminal Services\' -Name 'MinEncryptionLevel' -Value '3' -PropertyType Dword