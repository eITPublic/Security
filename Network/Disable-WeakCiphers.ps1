﻿# download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/Test-IsAdmin.ps1')

if(Test-IsAdministrator){ 
  ## Disable Weak Cyphers
  Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Null' -name "Enabled" -value 0 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\DES 56/56' -name "Enabled" -value 0 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 40/128' -name "Enabled" -value 0 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40/128' -name "Enabled" -value 0 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56/128' -name "Enabled" -value 0 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 64/128' -name "Enabled" -value 0 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168/168" -name "Enabled" -value 0 -PropertyType "Dword"
}
else{
  Write-Warning "session is not running as administrator. Please start PowerShell as with an Administrator session"
}