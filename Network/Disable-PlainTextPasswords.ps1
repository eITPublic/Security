﻿## load resource
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## SV-52861r2_rule : The regsitry HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters\ needs to have the value name EnablePlainTextPassword with the value of 0 of type DWORD 
## Some non-Microsoft SMB servers only support unencrypted (plain text) password authentication.  Sending plain text passwords across the network, when authenticating to an SMB server, reduces the overall security of the environment.  Check with the vendor of the SMB server to see if there is a way to support encrypted password authentication.' 
Ensure-ItemProperty -LiteralPath 'HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters\' -Name 'EnablePlainTextPassword' -Value '0'