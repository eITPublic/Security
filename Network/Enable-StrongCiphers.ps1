﻿# download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/Test-IsAdmin.ps1')

if(Test-IsAdministrator){
  #Enable Strong Cyphers 
  Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 128/128" -name "Enabled" -value 1 -PropertyType "Dword"
  Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\AES 256/256" -name "Enabled" -value 1 -PropertyType "Dword"
}
else{
  Write-Warning "session is not running as administrator. Please start PowerShell as with an Administrator session"
}