﻿# download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## SV-52870r2_rule 
## The server message block (SMB) protocol provides the basis for many network operations. Digitally signed SMB packets aid in preventing man-in-the-middle attacks.  If this policy is enabled, the SMB server will negotiate SMB packet signing as requested by the client. The regsitry HKLM:\System\CurrentControlSet\Services\LanManServer\Parameters\ needs to have the value name EnableSecuritySignature with the value of 1 of type DWORD
Ensure-ItemProperty -LiteralPath 'HKLM:\System\CurrentControlSet\Services\LanManServer\Parameters\' -Name 'EnableSecuritySignature' -Value '1'