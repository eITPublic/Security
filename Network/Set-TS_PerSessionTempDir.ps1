﻿# download the supporting functions and bring into session
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Common/iregistry.ps1')

## SV-52900r1_rule : The regsitry HKLM:\Software\Policies\Microsoft\Windows NT\Terminal Services\ needs to have the value name PerSessionTempDir with the value of 1 of type DWORD 
## If a communal temporary folder is used for remote desktop sessions, it might be possible for users to access other users temporary folders.  If this setting is enabled, only one temporary folder is used for all remote desktop sessions.  Per session temporary folders must be established.
Ensure-ItemProperty -LiteralPath 'HKLM:\Software\Policies\Microsoft\Windows NT\Terminal Services\' -Name 'PerSessionTempDir' -Value '1' -PropertyType Dword