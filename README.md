All scripts placed in the eITPublic Repository will be made available to general public.

# PowerShell Assume NT System
Run following to open up a powershell console under the NT System account.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/assume-ntsystem.ps1')
```


# POWERSHELL STIGs
---
The following scripts are setup to implement security configurations through simple PowerShell one-liners. The scripts are as-is.

## Network
---
### Disable-WeakCiphers.ps1
The configuration script will set the registry to disable weak schannel ciphers. Additionally SSL 2.0 and 3.0 will be disabled. Running the configuration requires PowerShell session opened as administrator. After successful configuraiton, the system will need to be rebooted.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Disable-WeakCiphers.ps1')
```

### Disable-SSL2.ps1
SSL 2.0 will be disabled. Running the configuration requires PowerShell session opened as administrator. After successful configuraiton, the system will need to be rebooted.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Disable-SSL2.ps1')
```

### Disable-SSL3.ps1
SSL 3.0 will be disabled. Running the configuration requires PowerShell session opened as administrator. After successful configuraiton, the system will need to be rebooted.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Disable-SSL3.ps1')
```

### Disable-TLS1.ps1
The configuration script will set the registry to disable TLS 1.0. After successful configuraiton, the system will need to be rebooted.

Note: not every system should have TLS 1.0 disabled. Applications such as MS SQL Server require TLS 1.0 by default and would need additional service packs applied prior to disabling. MS SQL Server 2016 is safe to disable TLS 1.0.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Disable-TLS1.ps1')
```


### Disable-PlainTextPasswords.ps1 | SV-52861r2_rule
Some non-Microsoft SMB servers only support unencrypted (plain text) password authentication. Sending plain text passwords across the network, when authenticating to an SMB server, reduces the overall security of the environment.  Check with the vendor of the SMB server to see if there is a way to support encrypted password authentication. The regsitry HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters\ needs to have the value name EnablePlainTextPassword with the value of 0 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Disable-PlainTextPasswords.ps1')
```
### Set-LmCompatibilityLevel.ps1 | SV-52865r1_rule
The Kerberos v5 authentication protocol is the default for authentication of users who are logging on to domain accounts.  NTLM, which is less secure, is retained in later Windows versions  for compatibility with clients and servers that are running earlier versions of Windows or applications that still use it.  It is also used to authenticate logons to stand-alone computers that are running later versions. The regsitry HKLM:\System\CurrentControlSet\Control\Lsa\ needs to have the value name LmCompatibilityLevel with the value of 5 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Set-LmCompatibilityLevel.ps1')
```
### Set-TS_MinEncryptionLevel.ps1 | SV-52899r2_rule
Remote connections must be encrypted to prevent interception of data or sensitive information. Selecting High Level will ensure encryption of Remote Desktop Services sessions in both directions. The regsitry HKLM:\Software\Policies\Microsoft\Windows NT\Terminal Services\ needs to have the value name MinEncryptionLevel with the value of 3 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Set-TS_MinEncryptionLevel.ps1')
```
### Set-TS_PerSessionTempDir.ps1 | SV-52900r1_rule
If a communal temporary folder is used for remote desktop sessions, it might be possible for users to access other users temporary folders.  If this setting is enabled, only one temporary folder is used for all remote desktop sessions.  Per session temporary folders must be established. The regsitry HKLM:\Software\Policies\Microsoft\Windows NT\Terminal Services\ needs to have the value name PerSessionTempDir with the value of 1 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/Network/Set-TS_PerSessionTempDir.ps1')
```


## System
---
### Disable-AutoAdminLogon.ps1 | SV-52107r2_rule
Allowing a system to automatically log on when the machine is booted could give access to any unauthorized individual who restarts the computer.  Automatic logon with administrator privileges would give full access to an unauthorized individual. The regsitry HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\ needs to have the value name AutoAdminLogon with the value of 0 of type String.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Disable-AutoAdminLogon.ps1')
```
### Disable-ShutdownWithoutLogon.ps1 | SV-52840r1_rule
Displaying the shutdown button may allow individuals to shut down a system anonymously.  Only authenticated users should be allowed to shut down the system.  Preventing display of this button in the logon dialog box ensures that individuals who shut down the system are authorized and tracked in the systems Security event log. The regsitry HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System\ needs to have the value name ShutdownWithoutLogon with the value of 0 of type DWORD. 
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Disable-ShutdownWithoutLogon.ps1')
```
### Restrict-AddPrintDrivers.ps1 | SV-52214r2_rule
 Allowing users to install drivers can introduce malware or cause the instability of a system.  Print driver installation should be restricted to administrators. The regsitry HKLM:\System\CurrentControlSet\Control\Print\Providers\LanMan Print Services\Servers\ needs to have the value name AddPrinterDrivers with the value of 1 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Restrict-AddPrintDrivers.ps1')
```
### Restrict-AnonymousLogonUsers.ps1 | SV-52847r1_rule
Allowing anonymous logon users (null session connections) to list all account names and enumerate all shared resources can provide a map of potential points to attack the system. The regsitry HKLM:\System\CurrentControlSet\Control\Lsa\ needs to have the value name RestrictAnonymous with the value of 1 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Restrict-AnonymousLogonUsers.ps1')
```
### Set-CachedLogonsCount.ps1 | SV-52846r2_rule
The default Windows configuration caches the last logon credentials for users who log on interactively to a system.  This feature is provided for system availability reasons, such as the users machine being disconnected from the network or domain controllers being unavailable.  Even though the credential cache is well-protected, if a system is attacked, an unauthorized individual may isolate the password to a domain user account using a password-cracking program and gain access to the domain. The regsitry HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\ needs to have the value name CachedLogonsCount with the value of 4 of type String
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Set-CachedLogonsCount.ps1')
```
### Set-EnabledForcedLogoff.ps1 | SV-52860r1_rule
Users must not be permitted to remain logged on to the network after they have exceeded their permitted logon hours.  In many cases, this indicates that a user forgot to log off before leaving for the day.  However, it may also indicate that a user is attempting unauthorized access at a time when the system may be less closely monitored.  Forcibly disconnecting users when logon hours expire protects critical and sensitive network data from exposure to unauthorized personnel with physical access to the computer. The regsitry HKLM:\System\CurrentControlSet\Services\LanManServer\Parameters\ needs to have the value name EnableForcedLogoff with the value of 1 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Set-EnabledForcedLogoff.ps1')
```
### Disable-CAD.ps1 | SV-52866r1_rule
Disabling the Ctrl+Alt+Del security attention sequence can compromise system security.  Because only Windows responds to the Ctrl+Alt+Del security sequence, a user can be assured that any passwords entered following that sequence are sent only to Windows.  If the sequence requirement is eliminated, malicious programs can request and receive a users Windows password.  Disabling this sequence also suppresses a custom logon banner. The regsitry HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System\ needs to have the value name DisableCAD with the value of 0 of type DWORD.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/eITPublic/Security/raw/master/System/Disable-CAD.ps1')
```

