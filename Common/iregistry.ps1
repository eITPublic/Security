﻿## Private Repo
function Ensure-RegistryPath {
<#
.Synopsis
   Idempotent function to create registry paths
.DESCRIPTION
   Idempotent function to gracefully create registry
   paths if they do not exist. If they do exists no
   modification will be made to the system. This function
   is designed to be run over and over again without
   on a system and only to modify when registry is 
   found not be as specified. The function ensures
   the path exists and is not specific to create
   update, add, etc.
.EXAMPLE
   Ensure-RegistryPath -Path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2"
#>
  [CmdletBinding()]
  PARAM(
    # Path of the registry to exist
    [Parameter(Mandatory=$true,
               ValueFromPipelineByPropertyName=$false,
               Position=0)]
    [string]$Path
    )
  if(test-path $Path -ErrorAction SilentlyContinue){
    Write-Verbose "No Change: Path already exists: $Path"
  }
  else {
    try{
      New-Item -Path $Path -ItemType Directory -Force -ErrorAction Stop | Out-Null
      Write-Verbose "Change: Path created: $Path"
    }
    catch{
        Write-Error "Failed to make Registry Path: $Path" -ErrorAction Stop
    }
  }
}#close Ensure-RegistryPath

function Ensure-ItemProperty{
<#
.Synopsis
   Idempotent function to create registry item properties
.DESCRIPTION
   Idempotent function that states the state of a registry key
   only if the state is not was is given. If the registry path
   does not exist, it will create it. If the key does not exist
   it will create it. If the value is not what is specified,
   it will set it.
.EXAMPLE
   Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Null" -name "Enabled" -value 0 -PropertyType "Dword"
.EXAMPLE
   Ensure-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Client" -name "Enabled" -value 1 -PropertyType "DWord"
#>
  [CmdletBinding()]
  PARAM(
    # Literal Path of the registry to exist
    [Parameter(Mandatory=$true,
               ValueFromPipelineByPropertyName=$false,
               Position=0)]
    [string]$LiteralPath,
    # Name of the key to create
    [Parameter(Mandatory=$true,
               ValueFromPipelineByPropertyName=$false,
               Position=1)] 
    [string]$Name,
    # Value of the key.
    [Parameter(Mandatory=$true,
               ValueFromPipelineByPropertyName=$false,
               Position=2)]             
    [string]$Value,
    # Propery of the key.
    [Parameter(Mandatory=$true,
               ValueFromPipelineByPropertyName=$false,
               Position=3)]
    [ValidateSet("Dword" ,"String","Binary","Qword","MultiString","ExpandString")]
    [string]$PropertyType
  )
  # verify the registry path is created
  Ensure-RegistryPath -Path $LiteralPath -ErrorAction Stop
  # Get details for the registry item to make sure the item exists
  $Registry = Get-ItemProperty -Path $LiteralPath -Name $Name -EA SilentlyContinue
  $RegValue = $Registry.$Name
  if (!($Registry)){
    # item property does not exist though the registry path does. We need to create the property
    Write-Debug "The key '$Name' did not exist in '$LiteralPath'...creating '$Name' and setting the value to '$Value' of type '$PropertyType'."
    try{
      New-ItemProperty -Path $LiteralPath -Name $Name -PropertyType $PropertyType -Value $Value -ErrorAction Stop | Out-Null
      Write-Verbose "Change: Created key '$Name' on path '$LiteralPath' with value '$Value' of type '$PropertyType'."
    }
    catch{
      Write-Error "Failed to create registry '$LiteralPath' key '$Name' with value: '$Value'. Terminating configuration..." -ErrorAction Stop
    }
  }
  else{
    if($RegValue -NOTLIKE $Value){
        ## the item property did exist; however, the value was not correct
        Write-Debug "The item property '$Name' exists with value '$RegValue'. Setting value to '$Value'"
        try{
          Set-ItemProperty -Path $LiteralPath -Name $Name -Value $Value -ErrorAction Stop | Out-Null
          Write-Verbose "Change: The item property '$Name' exists with value incorrect '$RegValue'. Setting value to new value: '$Value'"
        }
        catch {
          Write-Error "Failed to set registry to new value: '$Value'. Terminating configuration..." -ErrorAction Stop
        }
    }
    else{
        # the registry path exists, the key exists, and the value is correct...all is well
        Write-Verbose "No Change: Registry not changed. The item property '$Name' exists with the specified value '$RegValue'"
    }
  }
}#close Ensure-ItemProperty